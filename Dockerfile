FROM golang:1.13.3-alpine AS build
ARG SERVICE
WORKDIR /go/src/github.com/kaperys/${SERVICE}

RUN apk update && apk add make git
COPY go.* ./
RUN go mod download
COPY . .
RUN make build

FROM alpine:latest
ARG SERVICE
ENV APP=${SERVICE}

RUN apk add --no-cache ca-certificates
COPY --from=build /go/src/github.com/kaperys/${SERVICE}/bin/${SERVICE} /app/${SERVICE}

EXPOSE 8080
ENTRYPOINT /app/${APP}
