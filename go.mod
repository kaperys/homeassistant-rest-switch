module github.com/kaperys/_/smart-devices/jake-api

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.7.0
)
