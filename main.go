package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

var (
	url        = os.Getenv("URL")
	token      = os.Getenv("TOKEN")
	entity     = os.Getenv("ENTITY")
	username   = os.Getenv("USERNAME")
	password   = os.Getenv("PASSWORD")
	entityType = os.Getenv("ENTITY_TYPE")
)

func main() {
	r := gin.Default()

	api := r.Group("/api", gin.BasicAuth(gin.Accounts{username: password}))

	api.GET("/", status)
	api.POST("/", toggle)

	r.Run()
}

func status(c *gin.Context) {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/states/%s", url, entity), nil)
	if err != nil {
		logrus.WithError(err).Error("could not create http request")
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logrus.WithError(err).Error("could not perform http request")
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		logrus.WithError(err).Error("could not read response body")
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	data := struct {
		State string `json:"state"`
	}{}

	err = json.Unmarshal(body, &data)
	if err != nil {
		logrus.WithError(err).Error("could not parse response body")
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(res.StatusCode, gin.H{"active": (data.State == "on")})
}

func toggle(c *gin.Context) {
	et := "light"
	if entityType != "" {
		et = entityType
	}

	body := struct {
		Active string `json:"active"`
	}{}

	err := c.ShouldBindJSON(&body)
	if err != nil {
		logrus.WithError(err).Error("could not read http body")
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	u := fmt.Sprintf("%s/services/%s/turn_on", url, et)
	if body.Active == "false" {
		u = fmt.Sprintf("%s/services/%s/turn_off", url, et)
	}

	req, err := http.NewRequest(http.MethodPost, u, strings.NewReader(fmt.Sprintf(`{"entity_id": "%s"}`, entity)))
	if err != nil {
		logrus.WithError(err).Error("could not create http request")
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logrus.WithError(err).Error("could not perform http request")
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	res.Body.Close()
	c.JSON(res.StatusCode, gin.H{"active": body.Active != "false"})
}
